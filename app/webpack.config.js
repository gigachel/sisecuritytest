var path = require('path');
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = function(env, options) {
  return {
    entry: [
      './js/index.js',
    ],
    output: {
      path: path.resolve(__dirname, '../dist'),
      filename: './js/bundle.js'
    },
    devtool: "source-map",
    module: {
      rules: [{
        test: /\.vue$/,
        loader: 'vue-loader'
      }, {
        test: /\.js$/,
        include: path.resolve(__dirname, 'js'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: [["@babel/preset-env", {
              targets: {
                browsers: ['last 2 version']
              },
              useBuiltIns: 'usage'
            }]]
          }
        }
      }, {
        test: /\.less$|\.css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: MiniCssExtractPlugin.loader
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            url: false
          }
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: [
              autoprefixer({
                browsers: ['last 2 version']
              }),
              options.mode === 'production' ? cssnano({
                preset: 'default',
              }) : function(){}
            ],
            sourceMap: true
          }
        }, {
          loader: 'less-loader',
          options: {
            paths: [
              path.resolve(__dirname, 'css')
            ],
            sourceMap: true
          }
        }]
      }]
    },
    resolve: {
      alias: {
        'vue$': options.mode === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js'
      }
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: './css/bundle.css'
      }),
      new CopyWebpackPlugin([{
        from: './images',
        to: './images'
      }, {
        from: './fonts',
        to: './fonts'
      }]),
      new VueLoaderPlugin(),
    ]
  };
};
