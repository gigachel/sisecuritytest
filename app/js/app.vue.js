import Vue from 'vue';

import InputCheckbox from './components/InputCheckbox.vue';
import InputRadio from './components/InputRadio.vue';
import InputNumber from './components/InputNumber.vue';
import JavascriptLevel from './components/JavascriptLevel.vue';
import ExpandTextarea from './components/ExpandTextarea.vue';

new Vue({
  el: '#app',
  components: {
    InputCheckbox: InputCheckbox,
    InputRadio: InputRadio,
    InputNumber: InputNumber,
    JavascriptLevel: JavascriptLevel,
    ExpandTextarea: ExpandTextarea,
  },
  data: {
    // infos: [
    //   { name: 'Полное ФИО', value: 'Милюк Станислав Геннадьевич' },
    //   { name: 'Год рождения', value: '1984' },
    //   { name: 'Место жительства', value: 'г. Сыктывкар, Россия' },
    //   { name: 'Скайп', value: 'milkstas' },
    //   { name: 'Почта', value: 'milkstas@gmail.com' },
    // ],
    info: {
      name: 'Милюк Станислав Геннадьевич',
      birthyear: 1984,
      city: 'г. Сыктывкар, Россия',
      skype: 'milkstas',
      email: 'milkstas@gmail.com'
    },
    skills: [
      { name: 'БЭМ/OOCSS', value: true },
      { name: 'Pug (Jade)', value: false },
      { name: 'Stylus/LESS/SASS', value: true },
      { name: 'Работаю с SVG', value: true },
      { name: 'Верстаю семантично', value: true },
      { name: 'Accessibility(A11Y)', value: false },
      { name: 'ES2015 / ES2016', value: true },
      { name: 'Gulp / GRUNT', value: false },
      { name: 'Webpack', value: true },
      { name: 'Дружу с WebGL', value: false },
      { name: 'jQuery', value: true },
      { name: 'Знаю / изучаю VUE', value: true },
      { name: 'Знаю / изучаю React', value: false },
      { name: 'Знаю / изучаю Node.js', value: true },
      { name: 'Использую Git', value: true },
      { name: 'С глазомером всё ок', value: true },
      { name: 'Читаю habr.com', value: true },
      { name: 'Я ленивый(-ая)', value: false },
      { name: 'У меня хороший английский', value: true },
    ],
    jsLevel: {
      percent: 91,
      options: [
        'Не владею',
        'Использую готовые решения',
        'Использую готовые решения и умею их переделывать',
        'Пишу сложный JS с нуля',
      ]
    },
    about: 'Люблю javascript. Знаю CSS. Ищу удаленную работу, желательно с официальным трудоустройством.',
    plans: {
      plan: 'Прокачиваться в JS',
      options: [
        'Верстать',
        'Прокачиваться в JS',
        'Менеджерство',
        'Другое',
      ]
    },
    date: '09.02.2019'
  },
  watch: {
    $data: {
      handler: function (data) {
        // var infos = data.infos.reduce(function (obj, item) {
        //   obj[item.name] = item.value;
        //   return obj;
        // }, {});
        var info = Object.keys(data.info).reduce(function (obj, itemKey) {
          obj[itemKey] = data.info[itemKey];
          return obj;
        }, {});
        var skills = data.skills.reduce(function (arr, item) {
          if (!item.value) return arr;
          else return arr.concat(item.name);
        }, []);
        var jsLevel = {};
        jsLevel.percent = data.jsLevel.percent;
        jsLevel.options = data.jsLevel.options;
        jsLevel.level = jsLevel.options[Math.floor(jsLevel.options.length * jsLevel.percent / 100)];
        var plans = {};
        plans.plan = data.plans.plan;
        plans.options = data.plans.options;

        console.log({
          info: info,
          skills: skills,
          jsLevel: jsLevel,
          about: data.about,
          plans: plans,
          date: data.date,
        }, "data");
      },
      deep: true
    }
  }
});
